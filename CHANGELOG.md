## [6.4.0] - 2018-08-16
### Lib
 - Packaged with `lib 6.4.1`

## [6.2.3] - 2018-06-06
### Added
 - Support for adPause and adResume

## [6.2.2] - 2018-05-17
### Changed
- Reporting error messages in error code
### Lib
- Packaged with `lib 6.2.6`

## [6.2.1] - 2018-04-23
### Fixed
- Stop views after error
### Lib
- Packaged with `lib 6.2.2`

## [6.2.0] - 2018-04-09
### Added
- Included throughput calculation
- Rendition for mp4 content
### Lib
- Packaged with `lib 6.2.0`

## [6.1.2] - 2018-02-12
### Fix
- IMA ads support
### Lib
- Packaged with `lib 6.1.12`

## [6.1.1] - 2018-02-08
### Fix
- Fixed live detection for live without ads

## [6.1.0] - 2018-02-06
### Lib
- Packaged with `lib 6.1.11`
### Added
- Ad adapter
### Fix
- Fixed issues with error detection and live detection

## [6.0.0] - 2017-07-24
### Added
- First release
- Packaged with `lib 6.0.4`
