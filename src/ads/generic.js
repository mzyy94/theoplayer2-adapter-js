var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var NativeAdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-THEOplayer2-ads'
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.currentTime
  },

  getTitle: function () {
    return "unknown"
  },

  /** Override to return video duration */
  getDuration: function () {
    if (this.player.ads.currentAdBreak) {
      return this.player.ads.currentAdBreak.ads[this.adCount - 1].duration
    }
    return this.player.duration
  },

  /** Override to return resource URL. */
  getResource: function () {
    if (this.player.ads.currentAdBreak) {
      return this.player.ads.currentAdBreak.ads[this.adCount - 1].mediaFiles[0].resourceURI
    }
    return null
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    if (!this.plugin.getAdapter().flags.isJoined) {
      return 'pre'
    }
    if (this.plugin.getAdapter().getDuration() &&
      (this.plugin.getAdapter().getPlayhead() + (this.plugin._ping.interval / 1000) > this.plugin.getAdapter().getDuration())) {
      return 'post'
    }
    return 'mid'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player.ads)
    this.monitorPlayhead(true, false)

    // Register listeners
    this.references = []
    this.references['adbegin'] = this.playAdListener.bind(this)
    this.references['adend'] = this.endedAdListener.bind(this)
    this.references['aderror'] = this.errorAdListener.bind(this)
    this.references['adskip'] = this.skipAdListener.bind(this)
    this.references['adbreakbegin'] = this.resetCounterListener.bind(this)

    this.playerReferences = []
    this.playerReferences['pause'] = this.pauseAdListener.bind(this)
    this.playerReferences['playing'] = this.resumeAdListener.bind(this)
    this.playerReferences['play'] = this.resumeAdListener.bind(this)

    for (var key in this.references) {
      this.player.ads.addEventListener(key, this.references[key])
    }

    for (var key in this.playerReferences) {
      this.player.addEventListener(key, this.playerReferences[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    this.monitor.stop()

    // unregister listeners
    if (this.player.ads && this.references) {
      for (var key in this.references) {
        this.player.ads.removeEventListener(key, this.references[key])
      }
      this.references = []
    }

    if (this.player && this.playerReferences) {
      for (var key in this.playerReferences) {
        this.player.removeEventListener(key, this.playerReferences[key])
      }
      this.playerReferences = []
    }
  },

  /** Listener for 'adbegin' event. */
  playAdListener: function (e) {
    this.adCount++
    this.plugin._adapter.fireInit();
    this.fireStart()
    this.fireJoin()
    this.duration = this.getDuration()
  },

  /** Listener for 'aderror' event. */
  errorAdListener: function (e) {
    this.fireError()
  },

  /** Listener for 'adend' event. */
  endedAdListener: function (e) {
    this.fireStop({ adPlayhead: this.duration })
  },

  /** Listener for 'adskip' event. */
  skipAdListener: function (e) {
    this.fireStop({ skipped: true })
  },

  pauseAdListener: function (e) {
    this.firePause()
  },

  resumeAdListener: function (e) {
    this.fireResume()
  },

  resetCounterListener: function (e) {
    this.adCount = 0
  }
})

module.exports = NativeAdsAdapter
