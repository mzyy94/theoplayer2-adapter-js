var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.TheoPlayer2 = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  getPlayrate: function () {
    return this.player.playbackRate
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    if (this.plugin.getAdsAdapter() && this.plugin.getAdsAdapter().flags.isStarted) {
      return this.lastPlayhead || 0
    }
    if (this.player.currentTime != 0) {
      this.lastPlayhead = this.player.currentTime
    }
    return this.lastPlayhead || 0
  },

  getIsLive: function () {
    if (isNaN(this.player.duration)) return null
    return this.player.duration === Infinity
  },

  /** Override to return video duration */
  getDuration: function () {
    if (this.player.ads && this.player.ads.playing) return this.lastDuration || null
    this.lastDuration = this.player.duration
    return this.lastDuration
  },

  getThroughput: function () {
    if (this.downloadedList && this.downloadedList.length > 0) {
      return this.downloadedList.reduce(function (sum, a) { return sum + a }, 0) / (this.downloadedList.length)
    }
    return null
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    var qty = this.getActiveQuality()
    if (qty && qty.bandwidth) {
      return qty.bandwidth
    }
    return -1
  },

  /** Override to return rendition */
  getRendition: function () {
    var qty = this.getActiveQuality()
    if (qty) {
      if (qty.name && isNaN(qty.name)) {
        return qty.name
      } else if (qty.height && qty.width) {
        return youbora.Util.buildRenditionString(qty.width, qty.height, qty.bandwidth)
      } else if (qty.bandwidth && !isNan(qty.bandwidth)) {
        return youbora.Util.buildRenditionString(qty.bandwidth)
      }
    }
    if (this.player.videoHeight && this.player.videoWidth) {
      return youbora.Util.buildRenditionString(this.player.videoWidth, this.player.videoHeight, 0)
    }
    return -1
  },

  /** Override to return resource URL. */
  getResource: function () {
    if (this.player.src && !this.previousSrc) this.previousSrc = this.player.src
    return this.player.src || null
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    var version = 'THEOplayer'
    if (typeof THEOplayer !== 'undefined') {
      version += THEOplayer.version
    } else if (typeof theoplayer !== 'undefined') {
      version += theoplayer.version
    }
    return version
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'THEOplayer'
  },

  getActiveQuality: function () {
    var vTracks = this.player.videoTracks
    for (var i = 0; i < vTracks.length; i++) {
      var currentTrack = vTracks.item(i)
      if (currentTrack.enabled) {
        return currentTrack.activeQuality
      }
    }
    return {}
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    youbora.Util.logAllEvents(this.player)

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // Register listeners
    this.references = []
    this.networkReferences = []
    this.requestInterceptor = this.requestListener.bind(this)
    this.responseInterceptor = this.responseListener.bind(this)
    this.references['play'] = this.playListener.bind(this)
    this.references['playing'] = this.playingListener.bind(this)
    this.references['pause'] = this.pauseListener.bind(this)
    this.references['seeking'] = this.seekingListener.bind(this)
    this.references['ended'] = this.endedListener.bind(this)
    this.references['error'] = this.errorListener.bind(this)
    this.references['readystatechange'] = this.stateChangeListener.bind(this)
    this.networkReferences['offline'] = this.offlineListener.bind(this)
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
    if (this.player.network) {
      this.player.network.addRequestInterceptor(this.requestInterceptor)
      this.player.network.addResponseInterceptor(this.responseInterceptor)
      for (var key in this.networkReferences) {
        this.player.network.addEventListener(key, this.networkReferences[key])
      }
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
    if (this.player.network && this.networkReferences) {
      this.player.network.removeRequestInterceptor(this.requestInterceptor)
      this.player.network.removeResponseInterceptor(this.responseInterceptor)
      for (var key in this.networkReferences) {
        this.player.network.removeEventListener(key, this.networkReferences[key])
      }
      this.networkReferences = []
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireInit()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    if (!this.plugin.getAdsAdapter() || !this.plugin.getAdsAdapter().flags.isStarted) {
      this.fireResume()
      this.fireSeekEnd()
      this.fireBufferEnd()
      if (!this.player.ads || !this.player.ads.playing)
        this.fireStart()
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    if (!this.flags.isSeeking) {
      this.firePause()
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError(e.error, e.type)
    this.fireStop()
  },

  /** Listener for 'offline' event. */
  offlineListener: function (e) {
    this.fireError("OFFLINE_SOURCE")
    this.fireStop()
  },

  /** Listener for 'readystatechange' event. */
  stateChangeListener: function (e) {
    if (!this.plugin.getAdsAdapter())
      this.plugin.setAdsAdapter(new youbora.adapters.TheoPlayer2.NativeAdsAdapter(this.player))
    // Used when changing the source dinamically
    if (e.currentTime === 0 && e.readyState === 0 &&
      ((this.player.src != this.previousSrc) && (this.player.src != undefined))) {
      this.fireStop()
      this.previousSrc = this.player.src
    }
  },
  requestListener: function (e) {
    if (e.type === 'segment') {
      if (!this.downloadingList) {
        this.downloadingList = []
      }
      if (!this.downloadingList[e.url]) {
        if (!this.downloadingCount) this.downloadingCount = 0
        if (this.downloadingCount === 0) this.removeOldDownloads = true
        this.downloadingCount++
        this.downloadingList[e.url] = Date.now()
      }
    }
  },

  responseListener: function (e) {
    if (this.downloadingList && this.downloadingList[e.url]) {
      if (!this.simultaneous) this.simultaneous = 0
      this.simultaneous = Math.max(this.downloadingCount, this.simultaneous)
      var size = e.body.byteLength
      if (size) {
        if (!this.downloadedList || this.removeOldDownloads) {
          this.downloadedList = []
          this.removeOldDownloads = false
        }
        this.downloadedList.push(size * 8 * 1000 * this.simultaneous / (Date.now() - this.downloadingList[e.url]))
        // *1000 from ms to s, *8 in bytes
      }
      delete this.downloadingList[e.url]
      this.downloadingCount--
      if (this.downloadingCount === 0) this.simultaneous = 0
    }
  }
},
  // Static members
  {
    NativeAdsAdapter: require('./ads/generic'),
  }
)

module.exports = youbora.adapters.TheoPlayer2
